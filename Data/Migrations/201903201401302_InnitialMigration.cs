namespace Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InnitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cotizacion",
                c => new
                    {
                        IdCotizacion = c.Int(nullable: false, identity: true),
                        Monto = c.Decimal(precision: 18, scale: 2),
                        NombreCliente = c.String(),
                        Ruc = c.String(),
                        NombreVendedor = c.String(),
                        Email = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        DeletedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.IdCotizacion);
            
            CreateTable(
                "dbo.DetalleCotizacion",
                c => new
                    {
                        IdDetalleCotizacion = c.Int(nullable: false, identity: true),
                        IdProducto = c.Int(nullable: false),
                        IdCotizacion = c.Int(nullable: false),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Descuento = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Impuesto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Subtotal = c.Decimal(precision: 18, scale: 2),
                        Cantidad = c.Int(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        DeletedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.IdDetalleCotizacion);
            
            CreateTable(
                "dbo.Producto",
                c => new
                    {
                        IdProducto = c.Int(nullable: false, identity: true),
                        TipoProducto_Id = c.Int(nullable: false),
                        Precio = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Descripcion = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        DeletedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.IdProducto);
            
            CreateTable(
                "dbo.TiposProductos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Tipo = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        DeletedAt = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TiposProductos");
            DropTable("dbo.Producto");
            DropTable("dbo.DetalleCotizacion");
            DropTable("dbo.Cotizacion");
        }
    }
}
