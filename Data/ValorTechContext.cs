﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Domain;

namespace Data
{
    public class ValorTechContext : DbContext, IDbContext
    {
        public ValorTechContext() : base("ValorTechEntities")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<ValorTechContext>(null);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Cotizacion> Cotizacion { get; set; }
        public DbSet<DetalleCotizacion> DetalleCotizacion { get; set; }
        public DbSet<Producto> Producto { get; set; }
        public DbSet<TiposProductos> TiposProductos { get; set; }

        public IDbSet<TEntity> Set<TEntity>() where TEntity : class
        {
            return base.Set<TEntity>();
        }

        public int SaveChanges(bool guardarAuditoria)
        {
            return guardarAuditoria ? SaveChanges() : base.SaveChanges();
        }
    }
}
