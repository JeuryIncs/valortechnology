﻿using System.Linq;

namespace Data.Repositories
{
    public interface IBaseRepository<T>
    {
        T GetById(int id);
        IQueryable<T> GetAll();
        T Insert(T entity);
        T Update(T entity);
        T Update(T entity, int id);
        void SoftDelete(int id);
        void Delete(T entity);
        void Delete(int id);
        int Count();
    }
}
