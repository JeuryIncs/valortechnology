﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Domain;

namespace AppService.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> GetAll();
        TEntity GetById(object id); 
        TEntity Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);

        IQueryable<TEntity> Select(Expression<Func<TEntity, bool>> query);

        /// <summary>
        /// Incluye la entidad dada en el objeto que retorna la consulta
        /// </summary>
        /// <param name="nombreEntidad">Nombre de la entidad a incluir</param>
        /// <returns></returns>
        IQueryable<TEntity> Include(string nombreEntidad);

        /// <summary>
        /// Incluye la entidad dada en el objeto que retorna la consulta
        /// </summary>
        /// <param name="expresionInclude">Lambda que representa la entidad a incluir</param>
        /// <returns></returns>
        IQueryable<TEntity> Include(Expression<Func<TEntity, object>> expresionInclude);
    }
}
