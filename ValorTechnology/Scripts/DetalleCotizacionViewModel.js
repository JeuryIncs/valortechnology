﻿var DetalleCotizacionViewModel = function () {

    var self = this;

    self.IdProducto = ko.observable();
    self.Precio = ko.observable(0);
    self.Descuento = ko.observable(0);
    self.Impuesto = ko.observable(0);
    self.Cantidad = ko.observable(0);
    self.Subtotal = ko.observable();
    

    self.$Subtotal = ko.computed(function () {

        var valor = self.Cantidad() * self.Precio();
        var descuento = 0;

        if (self.Descuento() > 0)
             descuento = (valor * (self.Descuento() / 100));

        valor = valor - descuento;

        if (self.Impuesto() > 0 && valor > 0)
            valor = (valor * (self.Impuesto() / 100)) + valor;

        return self.Subtotal(parseFloat(valor).toFixed(2));
    });
};
 