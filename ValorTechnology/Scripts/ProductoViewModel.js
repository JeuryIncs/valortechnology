﻿var ProductoViewModel = function() {

    var self = this;

    self.IdProducto = ko.observable();
    self.Selected = ko.observable(false);
    self.Descripcion = ko.observable();
    self.Precio = ko.observable();
    self.TipoProducto_Id = ko.observable();
    self.DescripcionTipo = ko.observable();
};