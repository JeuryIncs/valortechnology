﻿var cotizacionViewModel = function () {

    var self = this;
    var idProducts;
    self.products = ko.observableArray();
    self.detalleCotizacion = ko.observableArray();
    self.total = ko.observable();
    self.NombreCliente = ko.observable();
    self.Email = ko.observable();
    self.Ruc = ko.observable();
    self.NombreVendedor = ko.observable();
    var $mymodal;
     
    self.addProducto = function () {

        idProducts = [];

        $.each(self.detalleCotizacion(), function (key, value) {
            idProducts.push(value.IdProducto());
        });
        
        $.ajax({
            url: '/Cotizaciones/ListadoProductos/',
            dataType: "json",
            data: { "idProductos": JSON.stringify(idProducts) }, 
            beforeSend: function () {
                // setting a timeout
                $(".loading").show();
            },
            contentType: "application/json; charset=utf-8",
            success: function (response) {

                $(".loading").hide();
                
                self.products.removeAll();

                $.each(response, function (key, value) {
                    self.products.push(ko.mapping.fromJS(value, { include: ['Selected'] }, new ProductoViewModel()));
                });

                $mymodal = $("#myModal");

                $mymodal.modal("show");
            }
        });
    };
    
    self.agregarProducto = function (data) {

        var productos = self.products().filter(function (item) {
            return item.Selected();
        });
        
        $.each(productos, function (key, value) {
            self.detalleCotizacion.push(ko.mapping.fromJS(value, { include: ['Descuento', 'Cantidad', 'Impuesto', ' Subtotal']}, new DetalleCotizacionViewModel()));
        });

        $mymodal.modal("hide");
    };

    self.crearCotizacion = function () {
         
        var data = {
            NombreCliente: self.NombreCliente(),
            Email: self.Email(),
            Ruc: self.Ruc(),
            NombreVendedor: self.NombreVendedor(),
            Monto: self.total(),
            DetalleCotizacion: self.detalleCotizacion()
        };

        $.ajax({
            url: '/Cotizaciones/Save/',
            type: "POST",
            data: { cotizacion : data}, 
            success: function (response) {
                window.location.href = "/Cotizaciones/Index/";
            } 
        });
    };

    self.totalCotizacion = ko.computed(function () { 
        if (self.detalleCotizacion().length > 0) {

            var total = self.detalleCotizacion().reduce((anterior, actual) =>
                parseFloat(anterior) + parseFloat(actual.Subtotal()), 0);

            self.total(parseFloat(total).toFixed(2));
        }
    });
};

ko.applyBindings(cotizacionViewModel, document.getElementById("cotizacion"));