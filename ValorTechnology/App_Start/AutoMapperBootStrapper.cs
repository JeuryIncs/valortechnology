﻿using AutoMapper;
using Domain;
using ValorTechnology.ViewModels;

namespace ValorTechnology.App_Start
{
    public class AutoMapperBootStrapper : Profile
    {
        public AutoMapperBootStrapper()
        {
            CreateMap<DetalleCotizacion, DetalleCotizacionViewModel>();
            CreateMap<Producto, ProductosViewModel>();
        }
    }
}