﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using AppService.Interfaces;
using AutoMapper;
using Domain;
using Microsoft.Ajax.Utilities;
using ValorTechnology.ViewModels;

namespace ValorTechnology.Controllers
{
    public class CotizacionesController : Controller
    {
        private IProductoService _productoService;
        private ITipoProductoService _tipoProductoService;
        private ICotizacionService _cotizacionService;
        private IDetalleCotizacionService _detalleCotizacionService;

        public CotizacionesController(IProductoService productoService,
            ITipoProductoService tipoProductoService,
            ICotizacionService cotizacionService,
            IDetalleCotizacionService detalleCotizacionService
            )
        {
            _productoService = productoService;
            _tipoProductoService = tipoProductoService;
            _cotizacionService = cotizacionService;
            _detalleCotizacionService = detalleCotizacionService;
        }
        
        public ActionResult Index()
        {
            ViewBag.Cotizaciones = _cotizacionService.GetAll().Count();

            ViewBag.Productos = _productoService.GetAll().Count();

            var listado = _cotizacionService.GetAll().ToList();

            return View(listado);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        public ActionResult Save(CotizacionViewModel cotizacion)
        {
            var newCotizacion = _cotizacionService.Insert(new Cotizacion
            {
                Monto = cotizacion.Monto,
                NombreCliente = cotizacion.NombreCliente,
                NombreVendedor = cotizacion.NombreVendedor,
                Ruc = cotizacion.Ruc,
                Email =  cotizacion.Email
            });

            cotizacion.DetalleCotizacion.ForEach(a =>
                {
                    _detalleCotizacionService.Insert(new DetalleCotizacion
                    {
                        Precio = a.Precio,
                        Descuento = a.Descuento,
                        IdProducto = a.IdProducto,
                        Impuesto = a.Impuesto,
                        Cantidad = a.Cantidad,
                        Subtotal = a.Subtotal,
                        IdCotizacion = newCotizacion.IdCotizacion
                    });
                }
            );

            return RedirectToAction("Index");
        }

        public JsonResult ListadoProductos(string idProductos)
        {
            var ids = new JavaScriptSerializer().Deserialize<IList<int>>(idProductos);

            var list = _productoService.GetAll().ToList();

            var productos = Mapper.Map<List<ProductosViewModel>>(list);

            productos.ForEach(a => { a.DescripcionTipo = _tipoProductoService.GetById(a.TipoProducto_Id).Tipo; });
            
            ids?.ForEach(a =>
            {
                productos = productos.Where(x => x.IdProducto != a).ToList();
            });

            return Json(productos, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Ver(int idCotizacion)
        {
            var cotizacion = _cotizacionService.GetAll().FirstOrDefault(x => x.IdCotizacion == idCotizacion);

            var detalle = _detalleCotizacionService.GetDetalleCotizaciones(idCotizacion);
            
            var cotizacionVm = new CotizacionViewModel
            {
                DetalleCotizacion = Mapper.Map<List<DetalleCotizacionViewModel>>(detalle),
                Email = cotizacion?.Email,
                NombreCliente = cotizacion?.NombreCliente,
                Ruc = cotizacion?.Ruc,
                Monto = cotizacion?.Monto,
                NombreVendedor = cotizacion?.NombreVendedor,
            };

            cotizacionVm.DetalleCotizacion.ForEach(a =>
                {
                    a.Producto = _productoService.GetById(a.IdProducto).Descripcion;
                });

            return View(cotizacionVm);
        }
    }
}