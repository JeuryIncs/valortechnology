﻿using System.Net; 
using System.Web.Mvc;
using AppService.Interfaces; 
using Domain;

namespace ValorTechnology.Controllers
{
    public class TiposProductosController : Controller
    {
        ITipoProductoService _tiposProductosService;

        public TiposProductosController(ITipoProductoService tiposProductosService)
        {
            _tiposProductosService = tiposProductosService;
        } 
       
        public ActionResult Index()
        {
            return View(_tiposProductosService.GetAll());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var tiposProductos = _tiposProductosService.GetById(id);

            if (tiposProductos == null)
            {
                return HttpNotFound();
            }
            return View(tiposProductos);
        }
        
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TiposProductos tiposProductos)
        {
            if (ModelState.IsValid)
            {
                tiposProductos.DeletedAt = null;
                _tiposProductosService.Insert(tiposProductos); 
                return RedirectToAction("Index");
            }

            return View("Create");
        }
         
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var tiposProductos = _tiposProductosService.GetById(id);

            if (tiposProductos == null)
            {
                return HttpNotFound();
            }

            return View(tiposProductos);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TiposProductos tiposProductos)
        {
            if (ModelState.IsValid)
            {
                _tiposProductosService.Update(tiposProductos);

                return RedirectToAction("Index");
            }
            return View(tiposProductos);
        }
         
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var tiposProductos = _tiposProductosService.GetById(id);

            if (tiposProductos == null)
            {
                return HttpNotFound();
            }

            return View(tiposProductos);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var tiposProductos = _tiposProductosService.GetById(id);

            _tiposProductosService.Delete(tiposProductos);

            return RedirectToAction("Index");
        }
    }
}
