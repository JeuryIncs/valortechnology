﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppService.Interfaces;
using AppService.Services;
using Data;
using Domain;

namespace ValorTechnology.Controllers
{
    public class ProductosController : Controller
    {
        ITipoProductoService _tiposProductosService;
        IProductoService _productoService;

        public ProductosController(ITipoProductoService tiposProductosService, IProductoService productoService)
        {
            _tiposProductosService = tiposProductosService;
            _productoService = productoService;
        }

        public ActionResult Index()
        {
            return View(_productoService.GetAll());
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var producto = _productoService.GetById(id);

            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        public ActionResult Create()
        {
            ViewBag.TiposProductos = _tiposProductosService.GetAll().Select(x => new SelectListItem { Text = x.Tipo, Value = x.Id.ToString() }).ToList();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Producto producto)
        {
            if (ModelState.IsValid)
            {
                producto.DeletedAt = null;
                _productoService.Insert(producto);
                return RedirectToAction("Index");
            }

            return View(producto);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var producto = _productoService.GetById(id);

            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Producto producto)
        {
            if (ModelState.IsValid)
            {
                _productoService.Update(producto);

                return RedirectToAction("Index");
            }
            return View(producto);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var producto = _productoService.GetById(id);

            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var producto = _productoService.GetById(id);

            _productoService.Delete(producto);

            return RedirectToAction("Index");
        }
    }
}
