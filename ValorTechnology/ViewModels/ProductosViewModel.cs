﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ValorTechnology.ViewModels
{
    public class ProductosViewModel
    {
        public int IdProducto { get; set; }
        public int TipoProducto_Id { get; set; }
        public decimal Precio { get; set; }
        public string Descripcion { get; set; }
        public string DescripcionTipo { get; set; }
    }
}