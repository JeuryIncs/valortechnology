﻿using System.Collections.Generic;
using Domain;

namespace ValorTechnology.ViewModels
{
    public class CotizacionViewModel
    {
        public int IdCotizacion { get; set; }

        public string NombreCliente { get; set; }
        
        public string Email { get; set; }

        public string Ruc { get; set; }

        public string NombreVendedor { get; set; }
        
        public decimal? Monto { get; set; }

        public List<DetalleCotizacionViewModel> DetalleCotizacion { get; set; }
    }
}