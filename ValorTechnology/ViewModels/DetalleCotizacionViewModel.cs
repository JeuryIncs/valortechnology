﻿
namespace ValorTechnology.ViewModels
{
    public class DetalleCotizacionViewModel
    {
        public int IdProducto { get; set; }
        public decimal Precio { get; set; }
        public int Cantidad { get; set; }
        public decimal Descuento { get; set; }
        public decimal Impuesto { get; set; }
        public decimal? Subtotal { get; set; }
        public string Producto { get; set; }
    }
}