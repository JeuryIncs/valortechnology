﻿using System; 

namespace Domain
{
    public class Entity
    {

        public bool IsActive { get; set; } = true; 

        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

        public DateTime? DeletedAt { get; set; } 
    }
}
