﻿using System.ComponentModel.DataAnnotations;
using System.Data.Linq.Mapping;

namespace Domain
{
    public class Producto : Entity
    {
        [Key]
        public int IdProducto { get; set; }
        public int TipoProducto_Id { get; set; }
        public decimal Precio { get; set; }
        public string Descripcion { get; set; }
    }
}
