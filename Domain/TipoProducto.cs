﻿namespace Domain
{
    public class TiposProductos : Entity
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
    }
}
