﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class DetalleCotizacion : Entity
    {
        [Key]
        public int IdDetalleCotizacion { get; set; }
        public int IdProducto { get; set; }
        public int IdCotizacion { get; set; }
        public decimal Precio { get; set; }
        public decimal Descuento { get; set; }
        public decimal Impuesto { get; set; }
        public decimal? Subtotal { get; set; }
        public int? Cantidad { get; set; }
        
    }
}
