﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Cotizacion : Entity
    {
        [Key]
        public int IdCotizacion { get; set; }
        public decimal? Monto { get; set; }
        public string NombreCliente { get; set; }
        public string Ruc { get; set; }
        public string NombreVendedor { get; set; }
        public string Email { get; set; }
    }
}
