﻿using System.Collections.Generic;
using System.Linq;
using AppService.Interfaces; 
using Domain;

namespace AppService.Services
{
    public class TipoProductoService : BaseService<TiposProductos>, ITipoProductoService
    { 
        public IList<TiposProductos> GetAll()
        {
            return Repository.GetAll()
                .Where(x => x.IsActive).ToList();
        }

        public TiposProductos GetById(int? id)
        {
            return Repository.GetById(id);
        }
         
    }
}
