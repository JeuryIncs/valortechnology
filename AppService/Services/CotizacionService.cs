﻿using System;
using System.Linq;
using AppService.Interfaces;
using Domain;

namespace AppService.Services
{
    public class CotizacionService : BaseService<Cotizacion>, ICotizacionService
    {
        public IQueryable<Cotizacion> GetAll()
        {
            return Repository.GetAll()
                .Where(x => x.IsActive);
        }

        public Cotizacion Insert(Cotizacion entity)
        {
           var cotizacion = Repository.Insert(entity);

            return cotizacion;
        }

        public void Update(Cotizacion entity)
        {
            Repository.Update(entity);
        }

        public void SoftDelete(int id)
        {
            var entity = GetById(id);

            if (entity == null)
                throw new Exception("Este objeto no existe");

            entity.DeletedAt = DateTime.UtcNow;
            entity.IsActive = false;

            Update(entity);
        }

        public void Delete(Cotizacion entity)
        {
            Repository.Delete(entity);
        }
    }
}
