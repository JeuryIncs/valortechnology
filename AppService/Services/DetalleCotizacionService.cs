﻿using System.Collections.Generic;
using System.Linq;
using AppService.Interfaces;
using Domain;

namespace AppService.Services
{
    public class DetalleCotizacionService : BaseService<DetalleCotizacion>, IDetalleCotizacionService
    {
        public List<DetalleCotizacion> GetDetalleCotizaciones(int idCotizacion)
        {
            return Repository.GetAll().Where(x => x.IdCotizacion == idCotizacion).ToList();
        }
    }
}
