﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using AppService.Interfaces;
using Data;
using Domain;

namespace AppService.Services
{
    public abstract class BaseService<T> : IService<T> where T : Entity
    {
        public IDbContext dbContext { get; set; }
        public IRepository<T> Repository { get; set; } 

        protected BaseService()
        {
            dbContext = new ValorTechContext();
            Repository = new RepositoryService<T>(dbContext);
        }

        #region Metodos Comunes

        public virtual void Insert(T model)
        {
            Repository.Insert(model);
            dbContext.SaveChanges();
        }

        public void Insert(bool guardarAuditoria, T model)
        {
            Repository.Insert(model);
            dbContext.SaveChanges(guardarAuditoria);
        }

        protected virtual void Insert(T model, bool salvar)
        {
            Repository.Insert(model);
            if (salvar) dbContext.SaveChanges();
        }

        public virtual void Update(T model)
        {
            Repository.Update(model);
            dbContext.SaveChanges();
        }

        public void Update(bool guardarCambios, T modelo)
        {
            dbContext.Entry(modelo).State = EntityState.Modified;

            if (guardarCambios) dbContext.SaveChanges();
        }

        public void SetValues(Object obj, T model)
        {
            dbContext.Entry(obj).CurrentValues.SetValues(model);
        }

        protected virtual void Update(T model, bool salvar)
        {
            Repository.Update(model);
            if (salvar) dbContext.SaveChanges();
        }

        public void Delete(T model)
        {
            Repository.Delete(model);
            dbContext.SaveChanges();
        }

        public void Delete(T model, bool salvar)
        {
            Repository.Delete(model);
            if (salvar) dbContext.SaveChanges();
        }

        public virtual IEnumerable<T> GetAll()
        {
            return Repository.GetAll();
        }

        public IQueryable<T> Select(Expression<Func<T, bool>> query)
        {
            return Repository.Select(query);
        }

        public T GetById(object id)
        {
            return Repository.GetById(id);
        }

        protected int SaveChanges()
        {
            return dbContext.SaveChanges();
        }

        #endregion

    }
}
