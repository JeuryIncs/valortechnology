﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppService.Interfaces;
using Domain;

namespace AppService.Services
{
    public class ProductoService : BaseService<Producto>, IProductoService 
    {
        public IList<Producto> GetAll()
        {
            return Repository.GetAll()
                .Where(x => x.IsActive).ToList();
        }

        public void SoftDelete(int id)
        {
            var entity = GetById(id);

            if (entity == null)
                throw new Exception("Este objeto no existe");

            entity.DeletedAt = DateTime.UtcNow;
            entity.IsActive = false;

            Update(entity);
        }
        
        public Producto GetById(int? id)
        {
            return Repository.GetById(id);
        }
         
    }
}
