﻿using System.Collections.Generic;
using Domain;

namespace AppService.Interfaces
{
    public interface IDetalleCotizacionService
    {
        void Insert(DetalleCotizacion entity);
         
        void Delete(DetalleCotizacion entity);

        List<DetalleCotizacion> GetDetalleCotizaciones(int idCotizacion);
    }
}
