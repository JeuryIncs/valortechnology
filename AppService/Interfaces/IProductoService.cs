﻿using System.Collections.Generic; 
using Domain;

namespace AppService.Interfaces
{
    public interface IProductoService
    {
        IList<Producto> GetAll();

        void Insert(Producto entity);

        void Update(Producto entity);

        void SoftDelete(int id);

        void Delete(Producto entity);

        Producto GetById(int? id);
    }
}
