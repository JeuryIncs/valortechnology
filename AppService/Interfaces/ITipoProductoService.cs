﻿using System.Collections.Generic;
using System.Linq; 
using Domain;

namespace AppService.Interfaces
{
    public interface ITipoProductoService 
    {
        IList<TiposProductos> GetAll();

        void Insert(TiposProductos entity);

        TiposProductos GetById(int? id);

        void Update(TiposProductos entity); 

        void Delete(TiposProductos entity);
    }
}
