﻿using System.Collections.Generic;
using Data;
using Domain;

namespace AppService.Interfaces
{
    interface IService<TEntity> where TEntity : Entity
    {
        IDbContext dbContext { get; set; }
        IRepository<TEntity> Repository { get; set; }
        void Insert(TEntity model);
        void Update(TEntity model);
        IEnumerable<TEntity> GetAll();
    }
}
