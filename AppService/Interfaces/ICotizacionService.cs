﻿using System.Linq; 
using Domain;

namespace AppService.Interfaces
{
    public interface ICotizacionService
    {
        IQueryable<Cotizacion> GetAll();

        Cotizacion Insert(Cotizacion entity);

        void Update(Cotizacion entity);

        void SoftDelete(int id);

        void Delete(Cotizacion entity);
    }
}
